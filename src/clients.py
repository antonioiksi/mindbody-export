import pandas as pd
import requests
import json
import csv
import sys


def flatten_json(y):
    out = {}

    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '_')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + '_')
                i += 1
        else:
            # TODO: replace comma with any symbol (for csv format)
            out[name[:-1]] = str(x)

    flatten(y)
    return out


def get_clients(headers, limit, offset):
    # url = "https://api.mindbodyonline.com/public/v6/client/clients?limit=10&offset=0"
    url = "https://api.mindbodyonline.com/public/v6/client/clients?limit={}&offset={}".format(
        limit, offset)
    payload = ''
    # payload = json.dumps({
    #     "limit": 10,
    #     "offset": 0
    # })
    try:
        print('requests.request("GET", {}, headers=headers, data=payload)'.format(url))
        response = requests.request("GET", url, headers=headers, data=payload)
    except Exception as ex:
        print('Ошибка выполнения запроса: {}'.format(ex))
    response.encoding = 'utf-8'
    response_json = json.loads(response.text)
    if 'Clients' in response_json:
        return response_json['Clients']
    else:
        print(json.dumps(response_json, indent=2, ensure_ascii=False))
        raise ValueError('Нет клиентов в ответе')


# Login and get access token
url = "https://api.mindbodyonline.com/public/v6/usertoken/issue?="

payload = json.dumps({
    "Username": "owner",
    "Password": "acdc1976"
})
headers = {
    'Api-Key': '602d96035268460db7e7dcb2f3c38077',
    'SiteId': '46949',
    'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)
response_json = json.loads(response.text)

print(json.dumps(response_json, indent=2))
accesstoken = response_json['AccessToken']
print(accesstoken)

# Get clients
headers['authorization'] = accesstoken

# limit must be not more than 200
limit = 200
offset = 0
total_clients = []
clients = get_clients(headers, limit, offset)
while (len(clients) > 0):
    total_clients.extend(clients)
    offset = offset + limit
    clients = get_clients(headers, limit, offset)

json_file = 'clients.json'
excel_file = 'clients.xlsx'
with open(json_file, 'w', encoding='utf-8') as f:
    json.dump(total_clients, f, ensure_ascii=False, indent=4)

df_json = pd.read_json(json_file)
df_json.to_excel(excel_file)


# # print(json.dumps(response_json, indent=4, ensure_ascii=False))
# # response_json.decode('utf-8')
# clients = response_json['Clients']
# new_clients = []
# for client in clients:
#     new_clients.append(flatten_json(client))

# # flatten_json = flatten_json(response_json)
# # print(json.dumps(new_clients, indent=2, ensure_ascii=False))

# f = open('data.csv', 'w')
# csv_file = csv.writer(f)
# csv_file.writerow(new_clients[0].keys())
# # x = x[1:]
# for item in new_clients[1:]:
#     try:
#         #     print(json.dumps(item, indent=2))
#         csv_file.writerow(item.values())  # ← changed
#     except Exception as e:
#         print(str(e))
# f.close()
