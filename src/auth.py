import requests
import json
import csv
import sys


def flatten_json(y):
    out = {}

    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '_')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + '_')
                i += 1
        else:
            # TODO: replace comma with any symbol (for csv format)
            out[name[:-1]] = str(x)

    flatten(y)
    return out


# Login and get access token
url = "https://api.mindbodyonline.com/public/v6/usertoken/issue?="

payload = json.dumps({
    "Username": "owner",
    "Password": "acdc1976"
})
headers = {
    'Api-Key': '602d96035268460db7e7dcb2f3c38077',
    'SiteId': '46949',
    'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)
response_json = json.loads(response.text)

print(json.dumps(response_json, indent=2))
accesstoken = response_json['AccessToken']
print(accesstoken)
