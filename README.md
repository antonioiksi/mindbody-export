# Export data from mindbody to csv

>**REQUIREMENTS**
>* Python 3+


pip install requests
pip install pandas openpyxl


## How to run
```sh
cd <project folder>

# run authentification
python3 src/auth.py

# Export clients into data.csv
python3 src/clients.py
```